public class Fluxo {
    public static void main(String[] args) {
        System.out.println("Inicio do main");
        try {
            metodo1();
        } catch(NullPointerException | ArithmeticException ex) {
            System.out.println("Exception: " +  ex.getMessage());
            ex.getStackTrace();
        }
        System.out.println("Fim main");
    }

    private static void metodo1() {
        System.out.println("Inicio metodo 1");
        metodo2();
        System.out.println("Fim metodo 1");
    }

    private static void metodo2() {
        System.out.println("Inicio metodo 2");
        for(int i = 1; i <= 5; i++) {
            System.out.println(i);
            int a = i/0;
            Conta conta = null;
            conta.deposita();
        }
        System.out.println("Fim metodo 2");
    }
}
