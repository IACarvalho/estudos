# Java Exceções

## Pilhas de exceções
O java executas suas functions em pilhas(stacks) que são FILO (first to in last to out), quando uma função chama outra a primeira função fica embaixo esperando a segunda terminar a execução.

## Bloco try catch
* Código com chances de ocorrer yum erro, por exemplo dividir algo por zero, colocados dentro de um ploco try e com o possível erro sendo tratado no bloco catch (Pode ter mais de um bloco catch) fará com quê a aplicação não pare por causa desse erro.

* Qunado a pilha de execução tem mais de uma camada vai interrompendo a execução até encontrtar algum tratamentou ou encerrar a aplicação.
```java
try {
  Double number = 4;
  Double result = number/0;
}
catch(ArithmeticException ex) {
  System.out.println(ex);
}

```

* Posso usar um mesmo bloco catch para capturar mais de uma exceção, basta separar a classe de exceção por um pipe (|)

```java
try {
  // código com possével exceções
} catch(ArithmeticException | NullPointerException ex) {
  // tratamento
}
```

### Métodos importantes que são compartilhados por exceções
* `getMessage()`: Retorna a mensagem da classe
* `printStackTrace()`: Mostra o caminho percorrido na pilha de execução