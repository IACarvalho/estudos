package com.example.springboot.services;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import com.example.springboot.controllers.ProductController;
import com.example.springboot.models.ProductModel;
import com.example.springboot.repositories.ProductRepository;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    public ResponseEntity<List<ProductModel>> getAllProducts () {
        List<ProductModel> productsList = productRepository.findAll();

        if(!productsList.isEmpty()) {
            for(ProductModel product : productsList ) {
                UUID id = product.getId();

                product.add(linkTo(methodOn(ProductController.class).getOneProduct(id)).withSelfRel());
            }
        }

        return new ResponseEntity<List<ProductModel>>(productsList, HttpStatus.OK);
    }

    public ResponseEntity<ProductModel> getOneProduct(UUID id) {
        Optional<ProductModel> productObject = productRepository.findById(id);

        if(productObject.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        productObject.get().add(linkTo(methodOn(ProductController.class).getAllProducts()).withRel("Products List"));

        return new ResponseEntity<ProductModel>(productObject.get(), HttpStatus.OK);
    }

    public ResponseEntity<ProductModel> saveProduct(@Validated ProductModel product) {
        return new ResponseEntity<ProductModel>(productRepository.save(product), HttpStatus.CREATED );
    }

    public ResponseEntity<?> deleteProduct(UUID id) {
        Optional<ProductModel> productObject = productRepository.findById(id);

        if(productObject.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        productRepository.delete(productObject.get());
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    public ResponseEntity<ProductModel> updateProduct(UUID id, @Validated ProductModel product) {
        Optional<ProductModel> productObject = productRepository.findById(id);

        if(productObject.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        productObject.get().setName(product.getName());
        productObject.get().setValue(product.getValue());

        return new ResponseEntity<ProductModel>(productRepository.save(productObject.get()), HttpStatus.OK);
    }
}
