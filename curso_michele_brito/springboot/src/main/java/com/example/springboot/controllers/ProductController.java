package com.example.springboot.controllers;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.springboot.models.ProductModel;
import com.example.springboot.services.ProductService;

@RestController
public class ProductController {

    @Autowired
    ProductService service;

    @GetMapping("/product")
    public ResponseEntity<List<ProductModel>> getAllProducts () {
        return service.getAllProducts();
    }

    @GetMapping("/product/{id}")
    public ResponseEntity<ProductModel> getOneProduct(@PathVariable(value = "id") UUID id) {
        return service.getOneProduct(id);
    }

    @PostMapping("/product")
    public ResponseEntity<ProductModel> saveProduct(@RequestBody @Validated ProductModel product) {
        return service.saveProduct(product);
    }

    @DeleteMapping("/product/{id}")
    public ResponseEntity<?> deleteProduct(@PathVariable(value = "id") UUID id) {
        return service.deleteProduct(id);
    }

    @PutMapping("/product/{id}")
    public ResponseEntity<ProductModel> updateProduct(@PathVariable(value = "id") UUID id, @RequestBody @Validated ProductModel product) {
        return service.updateProduct(id, product);
    }
}
