# Curso de estrutura de dados com Java DIO

## Conceito de nó
É um espeço em memória que guarda um valor e o endereço para o próximo nó.
O último nó aponta para um valor *null*

## Generics Java
Generics são os diamonds operators `List<String> minhaLista = new List<>();`

* Evitar casting excessivo
* Evitar código redundantes
* Encontrar erros em tempo de compilação
* Introduzido desde o Java SE 5.0

## Wildcards
* Unknown Wildcards (Unbounded)
* Bounded Wildcart (Upper Bounded/Lower Bounded)

### Unknown Wildcard

```java
public void imprimeLista(List<?>lista) {
  for(Object obj: lsita) {
    System.out.println(obj);
  }
}
List<Aluno> minhaLista = new List<Aluno>();
imprimeLista(minhaLista);
```

### UpperBounded wildcard

```java
public void imprimeLista(List<? extends Pessoa> listaPessoas) {
  for(Pessoa p:listaPessoas) {
    System.out.println(p);
  }
}
List<Aluno>minhaLista = new List<Aluno>();
imprimeLista(minhaLista);
```

### Conveção
* **K** para `key`, exemplo:`Map<K,V>`
* **V** pata `Value`, exemplo:  `Map<K,V>`
* **E** para `Element` exemplo: `List<E>`
* **T** para `Type`, exemplo: `Collections#addAll`
* **?** quando genérico

# Pilhas
* LIFO (Las to In First to Out)
* O ultimo elemento que entra é o primerio elemento que sai

Diferente sa sequência de nós normais os nós das pilhas tem a ordem inversa, o primeiro nó aponta para null e cada novo nó aponta para o nó anterior. E também existe uma referÇencia que irá apontar para o ultimo nó.
`null <- no1 <-no2 <- no3 <- no4 <- ref.`

## Métodos
* **.top()** -> `No meuNo = pilha.top()` -> Iré retornar uma referência para o valor no topo da pilha
* **.pop()** -> `No meuNo = pilha.pop()` -> Irá retornar o valor do topo da pilha e removê-lo da pilha
* **.push()** -> `No meuNo = new No(); pilha.push(meuNo);` -> Adiciona um elemento no topo da pilha
* **.isEmpy()** -> returna true se a pilha estiver vazia.

# Fila
* FIFO (First to In First to out)
* O primeiro elemento que entra é o primneiro elemento a sair.

Na fila o nó sempre aponta para o nó subsequente, e o último aponta para null.
`refNo -> obj03 -> obj02 -> obj01 -> null`

## Principais métodos
* **.enqueue()** -> Acrescenta um no final da fila, esse objeto apontara para o objeto que veio antes dele e a referencia de entrada da fila apontara para ele
* **.dequeue()** -> O primeiro objeto da fila sera removido e o segundo passara a apontar para null
* **.isEmpty**

# Lista Encadeadas
Lista encadeadas funcionam muito semelhantes com as filas, mas métodos e inserção e métodos no geral são um pouco diferentes
`refNo -> [Object() refNo] -> [Object() refNo] -> [Object() refNo] -> null`
## Metodos lista encadeada
* .add() -> Para adicionar um objeto. Com a diferença que posso adicionar na posição que quiser
* .remove() -> Remove um objeto. Assim como o add posso passar a posição que quero remover
* .isEmpty() -> Verifica se a lista está vazia
* .get() -> Retorna a referência de um objeto de acordo com o índice. Mantendo o objeto da lista