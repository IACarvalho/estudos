package com.projeto.Pilhas;

public class NoPilha {
  
  private int data;
  private NoPilha refNo = null;

  public NoPilha() {}

  public int getData() {
    return data;
  }

  public void setData(int data) {
    this.data = data;
  }

  public NoPilha getRefNo() {
    return refNo;
  }

  public void setRefNo(NoPilha refNo) {
    this.refNo = refNo;
  }

  public NoPilha(int data) {
    this.data = data;
  }

  @Override
  public String toString() {
    return "NoPilha [data=" + data + "]";
  }
}
