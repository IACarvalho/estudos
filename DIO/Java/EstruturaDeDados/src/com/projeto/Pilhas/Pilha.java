package com.projeto.Pilhas;

public class Pilha {
  private NoPilha refNoEntradaPilha;

  public Pilha() {
    this.refNoEntradaPilha = null;
  }

  public NoPilha pop() {
    if(!this.isEmpty()) {
      NoPilha noPoped = this.refNoEntradaPilha;
      this.refNoEntradaPilha = refNoEntradaPilha.getRefNo();
      return noPoped;
    }

    return null;
  }

  public void push(NoPilha novoNo) {
    NoPilha noAuxiliar = this.refNoEntradaPilha;
    refNoEntradaPilha = novoNo;
    refNoEntradaPilha.setRefNo(noAuxiliar);
  }

  public NoPilha top() {
    return this.refNoEntradaPilha;
  }

  public boolean isEmpty() {
    return this.refNoEntradaPilha == null ? true : false;
  }

  @Override
  public String toString() {
    String returnString = "-------------\n";
    returnString += "   Pilhas\n";
    returnString += "-------------\n";

    NoPilha noAuxiliar = this.refNoEntradaPilha;
    while(true) {
      if(noAuxiliar == null) {
        break;
      }
      returnString += "[No{dado=" + noAuxiliar.getData() + "}]\n";
      noAuxiliar = noAuxiliar.getRefNo();
    }
    returnString += "-------------\n";
    return returnString;
  }
}
