package com.projeto.lista_encadeada;

public class Node<T> {
  
  private T object;
  private Node<T> next;

  public Node() {
    this.next = null;
  }

  public Node(T object) {
    this.next = null;
    this.object = object;
  }

  public Node(T object, Node<T> next) {
    this.object = object;
    this.next = next;
  }

  public T getObject() {
    return object;
  }

  public void setObject(T object) {
    this.object = object;
  }

  public Node<T> getNext() {
    return next;
  }

  public void setNext(Node<T> next) {
    this.next = next;
  }

  @Override
  public String toString() {
    return "No [object=" + object + "]";
  }

  public String toStringList() {

    StringBuilder text = new StringBuilder();
    text.append("No [object=" + object + "]");

    if(this.next != null)
      text.append("-->" + this.next.toString());
    else 
      text.append("--> null");

    return text.toString();
  }
}
