package com.projeto.lista_encadeada;

public class Main {
  public static void main(String[] args) {
    LinkedList<String> lista = new LinkedList<>();

    lista.add("teste1");
    lista.add("teste2");
    lista.add("teste3");
    lista.add("teste4");

    System.out.println(lista);

    System.out.println(lista.get(0));
    System.out.println(lista.get(1));
    System.out.println(lista.get(2));
    System.out.println(lista.get(3));

    System.out.println(lista.remove(3));
    System.out.println(lista);
  }
}
