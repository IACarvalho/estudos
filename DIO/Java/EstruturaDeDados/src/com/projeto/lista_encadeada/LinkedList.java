package com.projeto.lista_encadeada;

import javax.xml.validation.Validator;

public class LinkedList<T> {
  
  Node<T> entraceRefference;

  public LinkedList() {
    this.entraceRefference = null;
  }

  public void add(T object) {
    Node<T> newNode = new Node<>(object);
    if(this.isEmpty()) {
      this.entraceRefference = newNode;
      return;
    }

    Node<T> auxNode = this.entraceRefference;

    for(int i = 0; i <this.size()-1; i++) {
      auxNode = auxNode.getNext();
    }

    auxNode.setNext(newNode);
  }

  public T get(int index) {
    return getNode(index).getObject();
  }

  private Node<T> getNode(int index) {

    indexValidator(index);

    Node<T> auxNode = this.entraceRefference;
    Node<T> nodeToReturn = null;

    for(int i = 0; i <= index; i++) {
      nodeToReturn = auxNode;
      auxNode = auxNode.getNext();
    }

    return nodeToReturn;
  }

  public T remove(int index) {
    Node<T> pivotNode = this.getNode(index);

    if(index == 0 ) {
      this.entraceRefference = pivotNode.getNext();
      return pivotNode.getObject();
    }

    Node<T> previewNode = this.getNode(index - 1);
    previewNode.setNext(pivotNode.getNext());
    return pivotNode.getObject();
  }

  public int size() {
    int listSize = 0;

    Node<T> auxNode = this.entraceRefference;

    while(true) {
      if(auxNode == null)
        break;

      listSize++;
      if(auxNode.getNext() == null)
        break;
      
      auxNode = auxNode.getNext();
    }

    return listSize;
  }

  private void indexValidator(int index) {
    StringBuilder messag = new StringBuilder();

    if(index >= this.size()) {
      messag.append("The index " + index + " is out of range");
      messag.append(" The maximum range is " + (this.size()-1) + '!');
      throw new IndexOutOfBoundsException(messag.toString());
    }
  }

  public boolean isEmpty() {
    return this.entraceRefference == null;
  }

  @Override
  public String toString() {
    if(this.isEmpty()) {
      return "null";
    }
    StringBuilder message = new StringBuilder();
    Node<T> auxNode = this.entraceRefference;

    for(int i = 0; i < this.size(); i++) {
      message.append("[Node {content=" + auxNode.getObject() + "}] --> ");
      auxNode = auxNode.getNext();
    }
    message.append("null");

    return message.toString();
  }

  
}
