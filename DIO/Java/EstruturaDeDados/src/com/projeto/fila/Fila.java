package com.projeto.fila;

public class Fila<T> {
  
  private No<T> refNoEntrada;

  public Fila () {
    this.refNoEntrada = null;
  }

  public void enqueue(T object) {
    No<T> novoNo = new No(object);
    novoNo.setRefNo(this.refNoEntrada);
    this.refNoEntrada = novoNo;
  }

  public T first() {
    if(this.isEMpty()) {
      return null;
    }

    No<T> primeiroNo = this.refNoEntrada;
    while(true) {
      if(primeiroNo.getRefNo() != null) {
        primeiroNo = primeiroNo.getRefNo();
      } else {
        return (T) primeiroNo.getObject();
      }
    }
  }

  public T dequeue() {
    if(this.isEMpty()) {
      return null;
    }

    No<T> primeiroNo = this.refNoEntrada;
    No<T> noAuxiliar = this.refNoEntrada;
    while(true) {
      if(primeiroNo.getRefNo() != null) {
        noAuxiliar = primeiroNo;
        primeiroNo = primeiroNo.getRefNo();
      } else {
        noAuxiliar.setRefNo(null);
        return (T) primeiroNo.getObject();
      }
    }
  }

  public boolean isEMpty() {
    return this.refNoEntrada == null;
  }

  @Override
  public String toString() {
    StringBuilder stringRetorn = new StringBuilder();

    No<T> noAuxiliar = this.refNoEntrada;

    if(this.isEMpty()){
      stringRetorn.append("null");
    }
    else {
      while(true) {
        stringRetorn.append("[No{objeto="+noAuxiliar.getObject()+"}]--->");
        if(noAuxiliar.getRefNo() == null) {
          stringRetorn.append("null");
          break;
        }
        noAuxiliar = noAuxiliar.getRefNo();
      }
    }

    return stringRetorn.toString();
  }

  
}
