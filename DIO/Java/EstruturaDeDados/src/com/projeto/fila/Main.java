package com.projeto.fila;

public class Main {
  public static void main(String[] args) {
    
    Fila<String> minhaFila = new Fila<>();

    minhaFila.enqueue("Primeiro");
    minhaFila.enqueue("Segundo");
    minhaFila.enqueue("Terceiro");
    minhaFila.enqueue("Quarto");

    System.out.println(minhaFila);
    
    // Remove e retorna o primeiro No da fila
    System.out.println(minhaFila.dequeue());

    // Fila apos remover o primeiro No
    System.out.println(minhaFila);

    // Adicionando mais um objeto na fila
    minhaFila.enqueue("Ultimo");
    System.out.println(minhaFila);

    // Primerio objeto da fila
    System.out.println(minhaFila.first());
    // Como não removi, apenas mostrei, o objeto ainda deve permanecer na fila
    System.out.println(minhaFila);
  }
}
