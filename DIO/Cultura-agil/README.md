# Trabalhando em Equipes Ágeis
Resumo de tópicos feitos durante o curso da DIO
# Introdução à cultura ágil

## Valores do manifesto

1 - **Indivíduos e interações**, mais que processos e ferramentas
2 - **Software em funcionamento** mais que documentação abrangente
3 - **Colaboração com o cliente** mais que negociação de contrato
4 - **Responder a mudanças** mais que seguir um plano

## 12 princípios para uma gestão de projetos ágil

1 - **Dar valor ao cliente de forma contínua e atempada**: para isso, os projetos são compostos por múltiplas e breves iterações nas quais a equipa partilha os avanços com o cliente.
2 - **Adaptar-se às necessidades e ao ambiente**: por isso é tão importante receber a interação do cliente. Neste sentido, a colaboração com ele e a reatribuição de recursos e empenho para nos adaptarmos às circunstâncias estão acima do cumprimento de um contrato rígido e inamovível.
3 - **Realizar entregas frequentes**: através de demonstrações e revisões do trabalho periodicamente (normalmente, entre uma e quatro semanas).
4 - **Trabalhar em equipa**: pôr as pessoas no centro do projeto é uma das grandes contribuições do método Ágil. Por isso, a gestão de equipas multifuncionais é a chave para o sucesso, bem como partilhar experiências para que cada projeto cristalize numa aprendizagem para o conjunto da empresa.
5 - **Motivar as pessoas**: depois da definição de objetivos e objetivos secundários, dar autonomia aos membros da equipa para que sintam o projeto como próprio.
6 - **Comunicar-se de forma direta**: são preferíveis as reuniões presenciais ou através de videoconferências, acima das comunicações escritas e mais impessoais.
7 - **Que o serviço funcione**: seguindo um foco lean, é importante priorizar as atividades que contribuem com valor acrescentado.
8 - **Criar uma cadência de reuniões para gerar hábitos na equipa**: seguindo um fluxo de interação Scrum, é aconselhável organizar reuniões de planificação para detalhar tarefas, breves encontros diários para comunicar os avanços, reuniões de revisão e reuniões para fazer o balanço dos resultados e impulsionar a melhoria contínua.
9 - **Garantir a excelência técnica**: para proporcionar um valor acrescentado ao cliente ao mesmo tempo que otimizamos os recursos destinados a isso.
10 - **Simplificar os produtos e serviços que oferecemos**: desta forma, podemos realizar mudanças de maneira mais rápida.
11 - **Contar com equipas auto-organizadas**, de maneira a poderem escolher as atividades a realizar para alcançar os objetivos.
12 - **Melhorar de forma contínua**: depois de cada iteração, é conveniente rever o que funcionou, o que vale a pena modificar e como melhorar as relações com os outros membros da equipa e com o cliente.

## Caracteristicas de um time ágil
* Alinhado com o cliente e stakeholders
* Auto-organizado e responsável
* Multidissiplinar
* Entrega valor continuamente
* Está semrpe aprendendo
* Melhoria contínua
* Possui métricas e metas claras
* Unido

## Importância da agilidade no mundo da tecnologia
* Traz inovações
* Faz entregas rápidas e com qualidade
* E almenta a eficiência

### VUCA
* **V** -> Volatility (Volátil)
* **U** -> Uncertainty (Incerto) 
* **C** -> Complexity (COmplexo)
* **A** -> Ambiguity (Ambíguo)

# Gerenciamento de Projetos e Gestão Ágil

## Projeto x Processo
### Processo

* É recorrente e contínuo
* É replicável
* Geralmente é repetitivo e realizado regularmente

### Projeto
* Um esforço temporário para atingir um objetivo
* O resultado é exclusivo
* Tem início e fim bem definidos

#### Exemplos
* **Processp** -> `Processo de solicitação de compra de uma emrpesa;`
Você solicita ao responsãvel do setor de compras a compra de um notebook, ele faz os orçamentos, o gestor/diretor aprova a compra e ele realiza o pedido.
* **Projeto** -> `Projeto de desenvolvimento de um novo modelo de fdones de ouvido de uma marca;`
Esse projeto iniciará com o objetivo de criar um modelo de fone de ouvido e passará oekas fases do projeto até a entrega do produto, o novo fone.

## Modelo Ágil x Tradicional

* `Modelo tradicional cascata (waterfall)`
As fases do projeto serão feitas de maneiras sequêncial e para que uma próxima etapa inicie a anterior tem que ser finalizada. O projeto é entregue apenas quando for finalizado.
`Anãlise -> Projeto (Design) -> Implementação -> Teste -> Entrega/Manutenção`

* `Modelo ágil com Scrum`
A principal diferença é que o projeto é feito em ciclo (sprints) com pequenas entregas contínuas, e em cada ciclo tem *Planejamento e Estimar*, *Implementar* e *Revisão e Retrospectiva*. Ao final de algums ciclos tem a entreaga.

## O que é Gestão Ágil?
Uma forma de gestão que seja:
* Flexível e que se adapte às mundanças;
* Colaborativa e que gere transparência;
* Voltada ao aprendizadop;
* Entrega contínua de valor.

# Métodos e ferramentas Ágeis

## O que é Scrum?
>Scrum é um *framework* leve que ajuda pessoas, times e organizações a gerar valor por meio de soluções *adaptativas* para *problemas complexos*. `Scrum Guide(Nov. 2020)`

## As bases do Scrum

`Empirismo`
Você tomará decisões ocm base em obsevações e aprendizados

`Lean Thinking`
Melhoria contínua e evitar desperdícios

## Pilares do Scrum
`Transparência`
A transparência permite que todos os ângulos de qualquer processo Scrum sejam obsevados por qualquer pessoa. Isto promove um fluxo de informação fácil e transparente em toda a organização e cria uma cultura de trabalho aberta.

`Inspenção`
A inspenção é a arte de pensar, no sentido aplicar uma visão crítica sobre o que está acontecendo. É dectectar variação ou problemas potencialmente indesejáveis.
* Olahndo e medindo o progresso do time
* Feedback dos clientes e stakeholders
* Inspeção e aprovação das entregas

`Adaptação`
A adaptação acontece quando o Time Scrum aprende através da transparência e da inspeção e, em seguida, adaptam o processo ao fazerem melhorias no trabalho que está sendo realizado, evitando, também, novos desvios.

## Framwork Scrum
> * O framework Scrum é propositalmente *incompleto*, apenas definindo as partes necessárias para implementar a teoria Scrum.
> * Ao invés de fornecer às pessoas *intruções detalhadas*, as regras do guia do Scrum orientam o seus relacionamentos e interações.
`Scrum Guide (Nov. 2020)`

## O que é XP?
**Extreme Programming (XP)** é o método ágil criado no final da década de 1990 pra desenvolvimento de software.
É uma metodologia que tem como objetivo crair sistemas com alta qualidade, com base em uma *interação próxima com os clientes, testagem constante e ciclos de desenvolvimento curto.*
O objetivo principal do XP é fazer *ciclos de entregas rápidos, contínuos e incrementais*, para atingir os resultados esperados pelo cliente.
`Planeja -> Faz -> Testa/Mensura -> Melhora/Incrementa -> Volta ao início`

### O que tem no XP?
* Ciclos
* "Cerimônias"
* Testes
* Programação em pares
* Feedback
* Aproximação com o cliente
* Flexibilidade

## Kanban
É um método para **definir, gerenciar e melhorar** serviços que entregam trabalho de conhecimento, como serviços profissionais, atividades criativas e o design de produtos físico e de software.
`A simplicidade e os benefícios que o método proporciona, faz dele um dos mais utilizados em emrpesas de todos os segmentos`

## OKR
Objetive Key Results -> É um *método de gestão ágil* que foi criado pelo ex-CEO da Intel Andrew Grove e tem como objetivo simplificar an forma de encarar os chamados os princiapis *objetivos estratégicos* de uma emrpesa.

**Pode ter certeza de que o método funciona, já que é usado no Google desde 1999.**

### Objetivos

Os objetivos do OKR são *descrições qualitativas* que apontam a direção que deverá ser seguida pela empresa.
* **Claro**
* **Inspirador**
* **Desafiador**
* **Alinhado com a missão do negócio**

Em alguns casos, os objetivos também podem trazer números (ex.: aumentar lucro em 10%), mas semrpe direcionados a um propósito mais amplo.

### Keys Results
Os resultados-chave, também chamados apenas de KRs, são as metas que determinam o antigimento do objetivo na metodologia OKR.

* **Baseadas em indicadores-chave de sesempenho (KPIs)**
* **Quantitativas**
* **SMART -> Specific(Específico), Measurable(Mensurável), Achievable(Atingível), Relevant(Relevante), Temporal(Temporais)**
`Assim, os Key Results servirão de referência para indicar o PROGRESSO em direção ao objetivo principal.`
### Benefícios do OKR
* Agilidade
* Cooperação
* Transparência
* Monitoramento contínuo
